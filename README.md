# Tuten UI Test

## Pre requisitos y Versiones
- Version de NPM: `6.14.18`
- Versión de Node: `14.15.1`


## Pasos para instalación
- Clonar repositorio desde: [https://gitlab.com/danielfutrille/toten-ui-test.git](https://gitlab.com/danielfutrille/toten-ui-test.git)
- Desde el directorio raiz (tuten-ui-test) ejecutar el siguiente comando: `npm install`
- Al finalizar, ejecutar el siguiente comando: `npm start`

El servicio se iniciará por defecto en `http://localhost:3000`

## Consideraciones
- El filtro por Booking Id se comporta como un "LIKE"
- La columna "Precio" se expresa con punto (.) el separador de miles y con coma (,) el separador de decimales, con un máximo de 2 decimales.
- La columna "Fecha de Creación" tiene el siguiente formato: {dia}-{mes}-{año}
- El valor de la columna "Cliente" (firstName y lastName) se toma de la clave "tutenUserClient" de un registro Booking.
- El valor de la columna "Dirección" (streetAddress) se toma de la clave "locationId" de un registro Booking.

