import axios from "axios";
import Auth from "../Auth";


export function signIn(email, password, data) {
    return (dispatch) => {
        return axios.put(`https://dev.tuten.cl/TutenREST/rest/user/${email}`, data, {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'password': password,
                'app': 'APP_BCK',
            }, data
        });
    };
}

export function getBookings(email) {
    const user = Auth.getCurrentUser();
    const token = Auth.getToken();
    return (dispatch) => {
        return axios.get(`https://dev.tuten.cl/TutenREST/rest/user/${email}/bookings?current=true`, {
            headers: {
                'Accept': "application/json",
                'adminemail': user.email,
                'app': 'APP_BCK',
                'token': token,
            }
        });
    };
}

export function makeRequest(options) {
    
    return (dispatch) => {
        return axios({
            method: options.method,
            url: options.route,
            data: options.data ? options.data : {},
            headers: options.headers ? options.headers : {},
        });
    };
}
