import { isNullOrUndefined } from './components/Utils/Utils'
import { User } from './models/User';

class Auth {

    login(data: any, callback: () => {}) {
        this.setIsAuthenticated(true);
        this.setToken(data.sessionTokenBck);

        this.setCurrentUser(data);
        if (typeof callback === 'function') {
            callback();
        }
    }

    setIsAuthenticated(value: boolean) {
        localStorage.setItem('isAuthenticated', value.toString());
    }

    getIsAuthenticated(): boolean {
        return localStorage.getItem('isAuthenticated') === 'true';
    }

    setToken(token: string) {
        localStorage.setItem('token', token);
    }

    getToken(): string | null {
        return localStorage.getItem('token');
    }

    setCurrentUser(user: User) {
        localStorage.setItem('currentUser', JSON.stringify(user));
    }

    getCurrentUser(): User {
        let user: User = null;
        let _user = localStorage.getItem('currentUser');
        if (!isNullOrUndefined(_user)) {
            user = JSON.parse(_user);
        }
        return user;
    }

    logout(callback: () => {}) {
        localStorage.clear();
        if (typeof callback === 'function') {
            callback();
        }
    }

    getLoginHomeUrl() {
        return '/';
    }

    getLoginUrl() {
        return '/login'
    }
}

export default new Auth();