import Swal from "sweetalert2";
import withReactContent from 'sweetalert2-react-content';
import Auth from "../../Auth";

const ReactSwal = withReactContent(Swal);

export function isNullOrUndefined(value) {
    return null === value || typeof value === 'undefined';
}

export function showMessageToast(message, options = {}) {
    Swal.fire({
        toast: true,
        position: !isNullOrUndefined(options.position) ? options.position : "top",
        icon: !isNullOrUndefined(options.icon) ? options.icon : "success",
        title: message ? message : "Exitoso",
        showConfirmButton: !isNullOrUndefined(options.showConfirmButton)
            ? options.showConfirmButton
            : false,
        timer: !isNullOrUndefined(options.timer) ? options.timer : 6000
    });
}

export function showMessage(
    message = "¡Acción culminada con éxito!",
    options = {}
) {
    return Swal.fire({
        icon: options.icon ? options.icon : "success",
        title: options.title ? options.title : null,
        text: message,
        showConfirmButton: options.showConfirmButton
            ? options.showConfirmButton
            : false
    });
}

export function showErrors(
    _message = "Error desconocido",
    errors
) {
    let status = 200;
    if (typeof _message === 'object') {
        if (!isNullOrUndefined(_message.response) && !isNullOrUndefined(_message.response.data)) {
            if (!isNullOrUndefined(_message.request)) {
                status = _message.request.status;
            }
            if (!isNullOrUndefined(_message.response.data.errors)) {
                _message = Object.values(_message.response.data.errors).join("<br />");
            } else if (!isNullOrUndefined(_message.response.data.message)) {
                _message = _message.response.data.message;
            }
        }
    }

    Swal.fire({
        icon: "warning",
        // title: "Oops...",
        html: _message,
        showConfirmButton: true,
        onClose: () => {
            // Usuario estando logeado fue: Bloqueado o No Verificado (por cambiar email)
            if (status === 423) {
                Auth.logout(() => {
                    window.location = '/';
                });
            }
            if (status === 412) {
                Auth.logout(() => {
                    window.location = '/login';
                });
            }
        }
    });

    return _message;
}

export function showLoadingMessage(message = "Cargando...", options = {}) {
    return Swal.fire({
        title: message,
        onBeforeOpen: () => {
            Swal.showLoading();
        },
        allowOutsideClick: () => !Swal.isLoading(),
        allowEscapeKey: () => !Swal.isLoading()
    });
}

export function showConfirmMessage(options = {}) {
    // const swalWithBootstrapButtons = Swal.mixin({
    //   customClass: {
    //     confirmButton: 'btn btn-outline-link',
    //     cancelButton: 'btn btn-danger'
    //   },
    //   buttonsStyling: false
    // })
    // swalWithBootstrapButtons.fire({
    //   title: '¿Está seguro(a)?',
    //   text: "Se eliminará toda su información",
    //   icon: 'warning',
    //   showCancelButton: true,
    //   focusConfirm: false,
    //   cancelButtonText: 'No, Cancelar',
    //   confirmButtonText: 'Sí, eliminar',
    //   reverseButtons: true,
    // })
    return Swal.fire({
        icon: "question",
        title: options.title ? options.title : "Confirmación",
        text: options.text ? options.text : '',
        html: options.html ? options.html : '',
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: options.confirmButtonText
            ? options.confirmButtonText
            : "Sí, ¡Eliminar!",
        cancelButtonText: options.cancelButtonText
            ? options.cancelButtonText
            : "Cancelar",
        focusConfirm: false,
        reverseButtons: true
    });
}

export function getSwal() {
    return ReactSwal;
}


export function hideMessages() {
    Swal.close();
}

export function log(...data) {
    if (process.env.REACT_APP_ENV !== "production") {
        console.log("[dev]", ...data);
    }
}
