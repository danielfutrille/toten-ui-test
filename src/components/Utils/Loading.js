import React, { Component } from "react";

export default class Loading extends Component {
    render() {
        const {
            show,
            containerClass,
            darkMode,
            containerStyle,
            size,
            color,
            title,
            titleClass,
        } = this.props;
        const mode = darkMode ? "dark" : "";
        const showLoading = !show ? "d-none" : "";

        return (
            <div
                className={`${showLoading} ${containerClass} overlay ${mode}`}
                style={containerStyle}
            >
                <i className={`fas fa-${size}x fa-spinner fa-spin text-${color}`}></i>
                {title !== '' && 
                    <div className={`${titleClass} text-${color}`}>{title}</div>
                }
            </div>
        );
    }
}

Loading.defaultProps = {
    containerClass: "row m-3 border-bottom p-3",
    darkMode: false,
    size: 4,
    color: 'primary',
    title: '',
    titleClass: 'h3 my-3',
};
