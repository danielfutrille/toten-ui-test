import { isNullOrUndefined } from "./Utils";

export const urlBase = process.env.REACT_APP_API_URL + "api/";

export const validateResponse = (response, callback) => {
    if (response.status === 200) {
        if (!isNullOrUndefined(callback) && typeof callback === 'function') {
            callback();
        }
        return response.data;
    } else {
        throw exeptionCodeResponse();
    }
};

export const exeptionCodeResponse = (error = "error") => {
    return error;
};
/*
export const getToken = () => {
  const local = localStorage.getItem("persist:root_cotizador_backoffice");
  const localParse = JSON.parse(local);
  return localParse.auth.token;
};

export const validateResponse = (response) => {
  if (response.ok) {
    return response.json();
  } else {
    throw exeptionCodeResponse();
  }
};

*/
