import React, { Component } from "react";
import { connect } from "react-redux";
import Content from "./Content";
import Header from "./Header";
import Auth from "../../Auth";
import Bookings from "../Bookings/Bookings";
import Login from "../Auth/Login";

window.React = React;

class App extends Component {
    constructor(props) {
        super(props);
        this.user = Auth.getCurrentUser();

        this.state = {
            redirect: false,
        };

    }

    getComponentFromUrl() {
        const { props } = this;
        let config = null;

        if (Auth.getIsAuthenticated()) {
            config = {
                component: <Bookings {...props} />
            };
        } else {
            config = {
                component: <Login {...props} />
            };
        }
        return config;
    }

    componentDidMount() {
    }

    render() {
        const { props } = this;
        let {
            component,
        } = this.getComponentFromUrl();

        return (
            <div id="body" className="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-not-fixed">
                <div className="wrapper">
                    <Header {...props} />
                    <Content {...props} component={component} />
                </div>
            </div>
        );
    }
}

function mapStateToProps(globalState) {
    return {
    }
}

const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
