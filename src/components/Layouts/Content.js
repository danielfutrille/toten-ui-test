import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';

class Content extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    render() {
        return (
            <Fragment>
                <div className="content-wrapper min-vh-100 bg-white">
                    <section className="content">
                        <div className="container">
                            {this.props.component}
                        </div>
                    </section>
                </div>
            </Fragment>
        )
    }
}


export default connect((state) => { return {} }, {})(Content);
