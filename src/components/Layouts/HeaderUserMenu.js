import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Auth from '../../Auth';

class HeaderUserMenu extends Component {
    constructor(props) {
        super(props);

        this.state = {}

        this.signOut = this.signOut.bind(this);
    }

    signOut(e) {
        e.preventDefault();
        e.stopPropagation();
        this.props.signOut();
    }

    render() {
        return (
            <Fragment>
                {/* USER MENU */}
                <ul className="navbar-nav ml-auto">
                    {Auth.getIsAuthenticated() ? (
                        <button onClick={this.signOut} className="btn btn-link text-primary">
                            Salir
                        </button>
                    ) : (
                        <Fragment>
                            <div className="d-block p-2">
                                <Link to="/login">Login</Link>
                            </div>
                        </Fragment>
                    )}
                </ul>
            </Fragment >
        )
    }
}

function mapStateToProps(globalState) {
    return {
    }
}

const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(HeaderUserMenu);
