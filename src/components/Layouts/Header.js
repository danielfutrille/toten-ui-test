import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Auth from '../../Auth';
import Modal from 'react-modal';
import HeaderUserMenu from './HeaderUserMenu';

Modal.setAppElement('#root1');

class Header extends Component {
    constructor(props) {
        super(props);

        this.state = {}

        this.signOut = this.signOut.bind(this);
        this.onClickTitle = this.onClickTitle.bind(this);

    }

    signOut() {
        Auth.logout();
        this.props.history.push('/');
    }

    onClickTitle(e) {
        this.props.history.push("/");
    }

    render() {
        return (
            <nav className="main-header navbar navbar-expand navbar-white navbar-light d-block" onClick={this.props.onClick}>
                <div className="row">
                    <div className="d-flex">
                        <div className="d-none d-sm-flex flex-row align-items-center ml-2" >
                            <Link to="/" className="" onClick={this.onClickTitle}>
                                <span className="font-weight-light" style={{ fontSize: '1.25rem', lineHeight: '1.5' }}>Tuten UI Test</span>
                            </Link>
                        </div>
                    </div>

                    <HeaderUserMenu {...this.props} signOut={this.signOut} />
                </div>
            </nav >
        )
    }
}

function mapStateToProps(globalState) {
    return {
        user: globalState.user,
    }
}

const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
