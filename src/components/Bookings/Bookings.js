import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';

import Auth from '../../Auth';
import { getBookings } from "../../routes/userRoutes";
import Loading from '../Utils/Loading';
import { log, showErrors } from '../Utils/Utils';
import NumberFormat from 'react-number-format';


class Bookings extends Component {
    constructor(props) {
        super(props);
        this.state = {
            authenticated: Auth.getIsAuthenticated(),
            user: Auth.getCurrentUser(),
            isLoading: false,
            bookings: [],
            filterType: 'booking',
            filterBookingId: '',
            filterPriceOperator: '>=',
            filterPrice: '',
        }

        this.onChange = this.onChange.bind(this);
        this.changeFilterPriceOperator = this.changeFilterPriceOperator.bind(this);
        this.changeFilterType = this.changeFilterType.bind(this);
    }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    changeFilterType(e) {
        this.setState({ filterType: this.state.filterType === 'booking' ? 'price' : 'booking' });
    }

    changeFilterPriceOperator() {
        this.setState({ filterPriceOperator: this.state.filterPriceOperator === '>=' ? '<=' : '>=' });
    }

    componentDidMount() {
        this.getBookings();
    }

    getBookings() {
        this.setState({ isLoading: true });
        this.props.getBookings('contacto@tuten.cl')
            .then((response) => {
                this.setState({
                    isLoading: false,
                    bookings: response.data
                });

            }, (errors) => {
                this.setState({ isLoading: false });
                showErrors(errors);
            })
            .catch((error) => {
                log('[CatchError]', Object.keys(error));
            });
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.filterPrice !== this.state.filterPrice) {
            if (isNaN(this.state.filterPrice)) {
                this.setState({ filterPrice: prevState.filterPrice });
            }
        }
    }

    render() {
        const {
            isLoading,
            bookings,
            filterBookingId,
            filterPriceOperator,
            filterPrice,
            filterType,
        } = this.state;

        let filteredBookings = bookings;
        if (filterBookingId !== '' || filterPrice !== '') {
            filteredBookings = bookings.filter((_b) => {
                return (
                    (filterType === 'booking' && _b.bookingId.toString().indexOf(filterBookingId) > -1) 
                    ||
                    (filterType === 'price' && (
                        (filterPriceOperator === '>=' && _b.bookingPrice >= parseFloat(filterPrice))
                        ||
                        (filterPriceOperator === '<=' && _b.bookingPrice <= parseFloat(filterPrice))
                    )) 
                );
            });
        }

        const children = filteredBookings.map((_booking, key) => {
            return (
                <BookingItem {...this.props}
                    key={_booking.id + '-' + key}
                    booking={_booking} />
            );
        });

        return (
            <div className="container container-wide pt-5 mt-5" >

                <div className="row mb-3">
                    <div className="col-12 text-left text-primary">
                        <h1>Bookings</h1>
                    </div>
                </div>

                <div className="row mb-3">
                    <div className="col-12 col-md-4 col-lg-3 text-primary mb-3">
                        Filtrar por: <button type="button" className="btn btn-default dropdown-toggle"
                            data-toggle="dropdown" aria-expanded="false">
                            <span>
                                <span className="ml-1 mr-3 font-weight-bold">
                                    {filterType === 'booking' ? 'Booking Id' : 'Precio'}
                                </span>
                            </span>
                        </button>
                        <div className="dropdown-menu" role="menu">
                            <button className="dropdown-item btn-link" onClick={(e) => this.changeFilterType()}>
                                {filterType === 'booking' ? 'Precio' : 'Booking Id'}
                            </button>
                        </div>
                    </div>
                    {filterType === 'booking' && (
                        <div className="col-12 col-md-4">
                            <div className="form-group row">
                                <div className="input-group">
                                    <input type="text" name="filterBookingId" className="form-control" onChange={this.onChange} />
                                </div>
                            </div>
                        </div>
                    )}
                    {filterType === 'price' && (
                        <div className="col-12 col-md-4">
                            <div className="form-group row">
                                <div className="input-group">
                                    <div className="input-group-prepend">
                                        <button type="button" className="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                            {filterPriceOperator === '>=' ? 'Precio >=' : 'Precio <='}
                                        </button>
                                        <ul className="dropdown-menu">
                                            <button className="dropdown-item btn-link" onClick={(e) => this.changeFilterPriceOperator()}>
                                                {filterPriceOperator === '>=' ? 'Precio <=' : 'Precio >='}
                                            </button>
                                        </ul>
                                    </div>
                                    <input type="text" name="filterPrice" value={filterPrice} className="form-control" onChange={this.onChange} />
                                </div>
                            </div>
                        </div>
                    )}
                </div>

                <div className="row">
                    <div className="col-12 text-center px-0">
                        <table className="table table-striped table-head-fixed">
                            <thead>
                                <tr>
                                    <th className="p-2" style={{ width: '5%' }}>BookingId</th>
                                    <th className="p-2 text-wrap text-center" style={{ width: '20%' }}>Cliente</th>
                                    <th className="p-2 text-wrap text-center" style={{ width: '15%' }}>Fecha de Creación</th>
                                    <th className="p-2 text-wrap text-center" style={{ width: '50%' }}>Dirección</th>
                                    <th className="p-2 text-wrap text-center" style={{ width: '10%' }}>Precio</th>
                                </tr>
                            </thead>
                            <tbody>
                                {!isLoading ? (
                                    (bookings.length > 0 ? (
                                        children
                                    ) : (
                                        <tr>
                                            <td className="col-12 h5 m-5 p-4 text-muted" colSpan={5}>
                                                No se encontraron bookings...
                                            </td>
                                        </tr>
                                    ))
                                ) : (
                                    <tr>
                                        <td className="p-0" colSpan={5}>
                                            <Loading show={isLoading} containerClass="p-5 text-center bg-white" />
                                        </td>
                                    </tr>
                                )}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        )
    }
}


export const BookingItem = (props) => {

    const { booking } = props;
    let date = new Date(booking.bookingTime);

    const formatted = ('0' + date.getUTCDay()).slice(-2) + '-' + ('0' + date.getUTCMonth()).slice(-2) + '-' + date.getUTCFullYear();

    return (
        <Fragment>
            <tr>
                <td className="p-2">
                    {booking.bookingId}
                </td>
                <td className="p-2 text-left">
                    {booking.tutenUserClient.firstName + ' ' + booking.tutenUserClient.lastName}
                </td>
                <td className="p-2 text-center">
                    {formatted}
                </td>
                <td className="p-2 text-left">
                    {booking.locationId.streetAddress}
                </td>
                <td className="p-2 text-right">
                    <NumberFormat
                        id="bookingPrice"
                        displayType="text"
                        type="text"
                        value={booking.bookingPrice}
                        name="bookingPrice"
                        thousandSeparator={"."}
                        decimalSeparator={","}
                        decimalScale={2}
                        fixedDecimalScale={true}
                        // isNumericString={true}
                    />
                </td>
            </tr>
        </Fragment >
    );
}

function mapStateToProps(globalState) {
    return {
    }
}

const mapDispatchToProps = {
    getBookings,
};

export default connect(mapStateToProps, mapDispatchToProps)(Bookings);
