import React, { Component, Fragment } from 'react'
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import Auth from '../../Auth';
import PropTypes from 'prop-types';
import { signIn } from '../../routes/userRoutes';
import classNames from 'classnames';
import SimpleReactValidator from 'simple-react-validator';
import { showErrors, log } from '../Utils/Utils';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            authenticated: Auth.getIsAuthenticated(),
            validated: false,
            email: '',
            password: '',
            isLoading: false,
        }
        this.validator = new SimpleReactValidator({
            locale: 'es',
            className: 'invalid-feedback d-block',
        });

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    onSubmit(e) {
        e.preventDefault();
        e.stopPropagation();
        this.setState({ validated: true });

        if (this.validator.allValid()) {
            this.setState({ isLoading: true });
            this.props.signIn(this.state.email, this.state.password, null)
                .then((response) => {
                    this.setState({ isLoading: false });
                    this.checkLogin(response.data);

                }, (errors) => {
                    this.setState({ isLoading: false });
                    showErrors(errors);
                })
                .catch((error) => {
                    log('[CatchError]', Object.keys(error));
                });
        } else {
            this.validator.showMessages();
            this.forceUpdate();
        }
    }

    checkLogin = (data) => {
        Auth.login(data, () => {
            this.setState(() => ({
                authenticated: true
            }));
        });
    };

    render() {
        const { authenticated, validated } = this.state;

        if (authenticated) {
            return (
                <Redirect to={Auth.getLoginHomeUrl()} />
            )
        }

        return (
            <Fragment>
                <div className="login-logo">
                    <Link to="/">Tuten UI Test</Link>
                </div>

                <div className="card">
                    <div className="card-body login-card-body">
                        <form noValidate onSubmit={this.onSubmit}>
                            <label htmlFor="email">Correo Electrónico</label>
                            <div className="input-group mb-3">
                                <input
                                    id="email"
                                    type="email"
                                    value={this.state.email}
                                    onChange={this.onChange}
                                    name="email"
                                    className={
                                        classNames("form-control", {
                                            'is-invalid': !this.validator.fieldValid('email') && validated,
                                            'is-valid': this.validator.fieldValid('email') && validated,
                                        })
                                    }
                                    placeholder="Correo Electrónico"
                                />
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-envelope" />
                                    </div>
                                </div>
                                {this.validator.message('email', this.state.email, 'required|email')}
                            </div>
                            <label htmlFor="password">Contraseña</label>
                            <div className="input-group mb-3">
                                <input
                                    id="password"
                                    type="password"
                                    value={this.state.password}
                                    onChange={this.onChange}
                                    name="password"
                                    className={
                                        classNames("form-control", {
                                            'is-invalid': !this.validator.fieldValid('password') && validated,
                                            'is-valid': this.validator.fieldValid('password') && validated,
                                        })
                                    }
                                    placeholder="Contraseña"
                                />
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-lock" />
                                    </div>
                                </div>
                                {this.validator.message('password', this.state.password, 'required,string')}
                            </div>

                            <div className="row mb-3">
                                <div className="col-12 col-md-6 col-lg-5 mb-3">
                                    <button
                                        className="btn btn-primary btn-block">
                                        Entrar
                                                {
                                            this.state.isLoading &&
                                            <i className="ml-2 fas fa-spinner fa-spin"></i>
                                        }
                                    </button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </Fragment>
        )
    }
}

Login.propTypes = {
    signIn: PropTypes.func.isRequired,
}


export default connect((state) => { return {} }, { signIn })(Login);