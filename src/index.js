import React, { Component } from "react";
import { render } from "react-dom";
import { Provider, connect } from "react-redux";
import {
    BrowserRouter as Router,
    Route,
    Switch,
    withRouter,
} from "react-router-dom";
import App from "./components/Layouts/App";
import { PersistGate } from "redux-persist/integration/react";
import { store, persistor } from "./redux/stores/store";

class Index extends Component {

    render() {
        return (
            <Switch>
                <Route exact path="/login" render={(props) => { return <App {...props} /> }} />
                <Route exact path="/" render={(props) => { return <App {...props} /> }} />
            </Switch>
        );
    }
}

export default connect(state => {}, {})(withRouter(Index));


const rootElement = document.getElementById("root1");
render(<Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
        <Router>
            <Index />
        </Router>
    </PersistGate>
</Provider>, rootElement);

// serviceWorker.unregister();
