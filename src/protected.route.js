import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import Auth from './Auth';


export const ProtectedRoute = ({ component: Component, render: Render, ...rest }) => (
    <Route {...rest} render={(props) => {
        if (Auth.getIsAuthenticated()) {
            return (<Component {...props} render={Render} />);
        } else {
            return (<Redirect to="/login" />);
        }
    }} />
)